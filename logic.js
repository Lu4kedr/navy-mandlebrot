
var xSlider;
var ySlider;
var zoomSlider;
var iterationSlider;

var iterationSliderText;
var zoomSliderText;
var xSliderText;
var ySliderText;


// nastaveni prvků p5.js
function setup() {

  createCanvas(500, 500);
  pixelDensity(1);
  
  var height=canvas.height;
  xSlider = createSlider(-10000, 10000, 0, 1);
  xSlider.position(0,height+50);
  xSliderText = createP();
  xSliderText.position(1,height+15);

  ySlider = createSlider(-10000, 10000, 0, 1);
  ySlider.position(0,height+100);
  ySliderText = createP();
  ySliderText.position(1,height+65);

  zoomSlider = createSlider(0.1,50,1,0.01);
  zoomSlider.position(0,height+150);
  zoomSliderText = createP();
  zoomSliderText.position(1,height+115);

  iterationSlider = createSlider(10,2000,50,1);
  iterationSlider.position(200,height+150);
  iterationSliderText = createP();
  iterationSliderText.position(200,height+115);
}

function draw() {
  // nacteni pole pixelu
  loadPixels();

  // postupna iterace skrz imaginarni cisla reprezentovana 2D souradnicemi canvasu (x -> realna cast, y-> imaginarni cast)
  for (var x = 0; x < width; x++) {
    for (var y = 0; y < height; y++) {

      //mapovani souradnic na rozsah ve kterem se imaginarni cisla budou pohybovat (usnadneni vypoctu)
      // doplnene o posunovani a zoomovani vysledku
      var z_real = map(x+xSlider.value(), 0, width*zoomSlider.value(), -1.8,2);
      var z_imaginar = map(y+ySlider.value(), 0, height*zoomSlider.value(), -1.8,2);

      var old_z_real = z_real;
      var old_z_imaginar = z_imaginar;

      // "nekonecna" sekvence limitována maximalnim poctem iteraci
      var n=0;
      for (n; n < iterationSlider.value(); n++) 
      {
        var new_z_real = z_real * z_real - z_imaginar * z_imaginar; // real
        var new_z_imaginar = 2 * z_real * z_imaginar; //complex

        z_real = new_z_real + old_z_real;
        z_imaginar = new_z_imaginar + old_z_imaginar;

        // ukoncovaci podminka, kdy hodnota komplenciho cisla presahne 2 (<=2)
        if (abs(z_real+z_imaginar) >= 2) {
          break;
        }
      }

      // podle vysledku sequnce nastavení jasu dané slozky pixelu
      // normalizace jasu
      var bright = map(n, 0, iterationSlider.value(), 0, 1);
      bright = map(sqrt(bright), 0, 1, 0, 255);

      // pokud probehly vsechny iterace "zhasneme" pixel
      if (n == iterationSlider.value()) {
        bright = 0;
      }

      var pix = (x + y * width) * 4;
      pixels[pix + 0] = bright; //red
      pixels[pix + 1] = bright-50; //green
      pixels[pix + 2] = bright-20; //blue
      pixels[pix + 3] = 255;
    }
  }

  updatePixels();
  xSliderText.html('X move');
  ySliderText.html('Y move');
  zoomSliderText.html('zoom');
  iterationSliderText.html('iteration');
}